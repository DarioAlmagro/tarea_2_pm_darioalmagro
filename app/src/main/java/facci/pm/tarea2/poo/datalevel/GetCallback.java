package facci.pm.tarea2.poo.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
